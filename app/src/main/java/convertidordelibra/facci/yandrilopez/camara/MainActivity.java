package convertidordelibra.facci.yandrilopez.camara;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button buttonCamara,buttonGuardar;
    ImageView imageViewFoto;
    EditText editTextFoto;
    Bitmap imageBitmap;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCamara = (Button)findViewById(R.id.buttonCamara);
        buttonGuardar = (Button)findViewById(R.id.buttonGuardar);
        imageViewFoto = (ImageView)findViewById(R.id.imageViewFoto);
        editTextFoto = (EditText)findViewById(R.id.editTextFoto);


       buttonCamara.setOnClickListener(this);
       buttonGuardar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
                GuardarImagen();
           }
       });
    }




    private static final int PERMISSION_SAVE = 101;
    String direccion = Environment.getExternalStorageDirectory().getAbsolutePath();


    private void GuardarImagen(){
        //preguntar si tenemos permiso a la SDcard
        if (tienePermisoSDcard()){
            //Crear carpeta
            //guardar el archivo
            CrearCarpeta();
           // String direccion = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+ "Imagen4A";
            direccion += "/Imagen4A/";
            GuardarFoto(direccion ,""+editTextFoto.getText(),imageBitmap);
        }else{
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_SAVE
                    );
        }

    }
    
    private void CrearCarpeta(){
        String nombreCarpeta = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+ "Imagen4A";
        File carpeta = new File(nombreCarpeta);
        if (!carpeta.exists()){
            if (carpeta.mkdir()){
                Toast.makeText(this,"Carpeta creada", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"Carpeta no fue creada", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void GuardarFoto(String direccionCarpeta, String nombreFoto,Bitmap bitmap){
        try{
            File imagen = new File(direccionCarpeta,nombreFoto);
            FileOutputStream stream = new FileOutputStream(imagen);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100,stream);
            stream.flush();
            stream.close();
            Toast.makeText(this,"FOTO GUARDADA" ,Toast.LENGTH_LONG).show();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    private boolean tienePermisoSDcard(){

        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }





    //obliga sobreescribir cuando se implementa el listener para llamar a la interfaz
    @Override
    public void onClick(View v) {
        //TODO: Llamar a la camara

        Intent tomarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (tomarFoto.resolveActivity(getPackageManager()) != null){
            startActivityForResult(tomarFoto, REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageViewFoto.setImageBitmap(imageBitmap);
        }

    }
}
